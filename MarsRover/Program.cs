﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MarsRoverService;

namespace MarsRover
{
    class Program
    {
        static void Main(string[] args)
        {
            Rover rover = new Rover();

            rover.SetDimenstion(5, 5);            
            //Rover 1
            rover.setPosition(1, 2, Direction.N);
            rover.Process("LMLMLMLMM");
            rover.printPosition();

            Console.WriteLine();
            //Rover 2:

            rover.setPosition(3, 3, Direction.E);
            rover.Process("MMRMMRMRRM");

            rover.printPosition();
            Console.Read();
        }
    }
}
