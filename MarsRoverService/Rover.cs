﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarsRoverService
{
    public class Rover
    {
        int length = 5;
        int height = 5;

        int x = 0;
        int y = 0;
        int facing = Direction.N;
        
        public void SetDimenstion(int length,int height)
        {
            this.length = length;
            this.height = height;
        }
        public void setPosition(int x, int y, int position)
        {
            this.x = x;
            this.y = y;
            this.facing = position;
        }

        private void LeftTurn()
        {
            facing = (facing - 1) < Direction.N ? Direction.W : facing - 1;
        }
        private void RightTurn()
        {
            facing = (facing +  1) > Direction.W ? Direction.N : facing +  1;
        }

        public void printPosition()
        {
            char dir = 'N';
            if (facing == 1)
            {
                dir = 'N';
            }
            else if (facing == 2)
            {
                dir = 'E';
            }
            else if (facing == 3)
            {
                dir = 'S';
            }
            else if (facing == 4)
            {
                dir = 'W';
            }
            Console.Write(x +" "+ y + " " + dir);

        }

        private void Move()
        {
            if (facing == Direction.N)
            {
                if(y +1 <= height)
                this.y++;
            }
            else if (facing == Direction.E)
            {
                if(x+1 <= length)
                this.x++;
            }
            else if (facing == Direction.S)
            {
                this.y--;
            }
            else if (facing == Direction.W)
            {
                this.x--;
            }
        }

        public void Process(string input)
        {
            foreach(char command in input)
            {

                if (command.Equals('L'))
                {
                    LeftTurn();
                }
                else if (command.Equals('R'))
                {
                    RightTurn();
                }
                else if (command.Equals('M'))
                {
                    Move();
                }
               

            }

          
        }
    }

    public static class Direction
    {
        public const int N = 1;
        public const int E = 2;
        public const int S = 3;
        public const int W = 4;
    }

   
}
